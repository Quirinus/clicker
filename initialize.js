//******** GLOBAL CONFIG *********************

var initialLives = 5;

var moveStep = 1; // px
var timeStep = 10; // milliseconds
var numberOfDropBoxes = 10;
var delayBetweenBoxesSpawningAtStart = 200; // milliseconds

//********* GLOBAL STATE AND ELEMENTS ********

var lives = initialLives;
var score = 0;

var dropBoxes = [];
var dropBoxesSpawnDelays = [];
var dropBoxesMoveIntervals = [];

var gameBox = ID('gameBox');

//*********** EVENTS AND START ***************

gameBox.addEventListener('contextmenu', function (event) {
	event.preventDefault(); // prevent right click menu
});
gameBox.addEventListener('dragstart', function (event) {
	event.preventDefault(); // prevent dragging
});
ID('restart').addEventListener('click', function () {
	start();
});

start();