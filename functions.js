function ID(id)
{
	return document.getElementById(id);
}

function createDropBox()
{
	var dropBox = document.createElement('div');
	dropBox.className = 'dropBox';

	dropBox.addEventListener('mousedown', function () {
		boxClick(dropBox);
	});
	dropBox.addEventListener('dragstart', function (event) {
		event.preventDefault(); // prevent dragging
	});
	dropBox.addEventListener('contextmenu', function (event) {
		event.preventDefault(); // prevent right click menu
	});

	gameBox.appendChild(dropBox);
	dropBoxes.push(dropBox);

	dropBoxResetPositionNoOverlap(dropBox);

	return dropBox;
}

function boxClick(dropBox)
{
	// leave if game is over, don't add score
	if (lives <= 0)
		return;

	dropBoxResetPositionNoOverlap(dropBox);

	score++;
	ID('score').innerHTML = score;
}

function dropBoxResetPositionNoOverlap(dropBox)
{
	// ******************************
	// this basically resets the box position to the top, and to a random position on the x axis
	// then it gets the position of the box, and compares it with all the other boxes
	// if the box overlaps another box, it resets its position and checks for overlapping again
	// it repeats this until its not overlapping any other box,
	// or until it fails "counterTryLimit" times, after which we spawn an overlapping box
	// ******************************

	// number of spawning tries, after which we allow overlapping for this spawn of the box
	var noOverlapSpawnTryLimit = 20;

	for (var i = 0; i < noOverlapSpawnTryLimit; i++)
	{
		resetPosition(dropBox);

		// if there's no overlapping, we exit sooner
		if (!doesSomeDropBoxOverlapWith(dropBox))
			break;
	}


	// yo dawg, these functions are used just here
	// so I put them inside this function
	// so they can function while they function

	function resetPosition(dropBox)
	{
		dropBox.style.left = getRandomLeftPos(dropBox) + 'px';
		dropBox.style.top = '0px';
	}

	function getRandomLeftPos(dropBox)
	{
		// we subtract dropBox width so that boxes don't spawn outside
		var widthAllowedSpawning = (gameBox.clientWidth - dropBox.offsetWidth);

		return Math.round(widthAllowedSpawning*Math.random());
	}

	function doesSomeDropBoxOverlapWith(dropBox)
	{
		var dBCoords = getDropBoxTopLeftBottomDown(dropBox);

		for (var i = 0; i < dropBoxes.length; i++)
		{
			// we don't need to check if the box overlaps with itself, so we continue to another box
			if (dropBox.isSameNode(dropBoxes[i]))
				continue;

			var dBCoordsOther = getDropBoxTopLeftBottomDown(dropBoxes[i]);

			if (doRectanglesOverlap(...dBCoords, ...dBCoordsOther))
				return true;
		}

		return false;
	}

	// gets the coordinates of the top-left and bottom-right corners of a box
	function getDropBoxTopLeftBottomDown(dropBox)
	{
		var x1 = dropBox.offsetLeft;
		var y1 = dropBox.offsetTop;
		var x2 = dropBox.offsetLeft + dropBox.offsetWidth;
		var y2 = dropBox.offsetTop + dropBox.offsetHeight;

		return [x1, y1, x2, y2];
	}

	function doRectanglesOverlap(a1x, a1y, a2x, a2y, b1x, b1y, b2x, b2y)
	{
		var aLeftOfB = a2x < b1x;
		var aRightOfB = b2x < a1x;
		var aAboveB = a2y < b1y;
		var aBelowB = b2y < a1y;

		return !(aLeftOfB || aRightOfB || aAboveB || aBelowB);
	}
}

function moveDropBoxAndHandleBottom(dropBox)
{
	var isOnBottom = (dropBox.offsetTop + dropBox.offsetHeight >= gameBox.clientHeight);

	// if box has not reached bottom:
	if (!isOnBottom)
	{
		dropBox.style.top = parseInt(dropBox.style.top) + moveStep + 'px'; // move box down by moveStep
		return;
	}

	// if box reached bottom:
	lives--;
	ID('lives').innerHTML = lives;

	if (lives > 0)
	{
		dropBoxResetPositionNoOverlap(dropBox);
		return;
	}

	// if no more lives:
	dropBox.style.backgroundColor = 'red'; // mark the sucker that did you in!
	gameOver();
}

function gameOver()
{
	// stop moving all boxes
	for (var i = 0; i < dropBoxesMoveIntervals.length; i++)
	{
		clearInterval(dropBoxesMoveIntervals[i]);
	}

	// turn off spawning any boxes that haven't been spawned yet
	for (var i = 0; i < dropBoxesSpawnDelays.length; i++)
	{
		clearTimeout(dropBoxesSpawnDelays[i]);
	}
}

function createMovingDropBox()
{
	var dropBox = createDropBox();

	// here we handle repeating box movement (falling down)
	var movingInterval = setInterval(moveDropBoxAndHandleBottom, timeStep, dropBox);
	dropBoxesMoveIntervals.push(movingInterval);
}

function start()
{
	// reset and set up everything:

	gameOver();

	for (var i = 0; i < dropBoxes.length; i++)
	{
		dropBoxes[i].remove(); // delete any old dropBoxes
	}

	lives = initialLives;
	score = 0;

	ID('lives').innerHTML = lives;
	ID('score').innerHTML = score;

	dropBoxes = [];
	dropBoxesSpawnDelays = [];
	dropBoxesMoveIntervals = [];

	// start everything:

	for (var i = 0; i < numberOfDropBoxes; i++)
	{
		var spawningDelay = delayBetweenBoxesSpawningAtStart*i; // each box spawns later than the previous one
		var spawningTimeout = setTimeout(createMovingDropBox, spawningDelay); // wait before spawning each box
		dropBoxesSpawnDelays.push(spawningTimeout);
	}
}